**EXAM PROJECT**

For your exam you must create a system that saves everything in text files. If you decided to use a database, you still need to show that you can use a text file to CRUD data. This means that at least 1 text file must be used.
NOTE: You will not get questions about databases  in your exam, so don’t use time in it.

The system:

One page and one page only

* Admin user
    * CRUD users
    * CRUD products (eg: name, price, pictures)
* Normal user/buyer
    * View products
    * Buy product
    * Signup (eg: name, password, picture)
    * Login (session)
    *Subscribe
    *Edit his/her own profile
* API based
* Localhost
* Desktop notification
* Sound
* Google maps with markers
* Subscribe to receive emails *
* Technology:
* JavaScript / JQuery
* PHP
* Data files
* Example of a scenario:
 
The system starts with no data. The first user is created as a “Admin” user. The admin can CRUD products and can CRUD other users.
Products can have a name, price, offer price (between two dates), etc...
An other person wants to use the system, so he/she browses the products and decides to buy it. When the purchase is done, the stock for that product decreases by 1.
The person visiting the page can subscribe to a newsletter/offers email. This person provides a name, password, geo-location (to be used with Google maps).
When the buyer “buys” a sound is played and the desktop notification pops-up.
* It is fine if the system does NOT send an email, but you need to create a system in which users can register for the newsletter/offer email
REMEMBER: Write a report based on the information from the module document