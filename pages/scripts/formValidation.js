function validateForm(inputs) {
    var validForm = true;
    for(var i = 0; i < inputs.length; i++){
        var error = "."+inputs[i].id+"Error";
        $(error).css("display", "none");
        if(inputs[i].val.length < inputs[i].minLength ){
            $(error).css("display", "block");
            validForm = false;
        }
        if(inputs[i].id === "email"){
            if(!validateEmail(inputs[i].val)){
                $(error).css("display", "block");
                validForm = false;
            }
        }

    }
    return validForm;
}

function editUserAdmin(email) {
    //clear form
    $("#AccName").val("");
    $("#AccLastName").val("");
    $("#AccEmail").val("");
    $("#AccPassword").val("");

    //show edit for user
    $(".editPage").css("display", "block");
    $(".EditHeader").text('Account page - edit '+email);
    //get user info
    getUserAdmin(email);


}
function setUsersToDashBoard(res){
    var ul = $("#usersUl");
    ul.html('');


    for(var i = 0; i<res.length; i++){

        var emailData= '\'' + res[i].email + '\'';
        var editMethod = " onclick=editUserAdmin("+emailData+")";
        var deleteMethod = " onclick=deleteUser("+emailData+")";

        var element = "<li>"+emailData+"</li>";
        var buttonEdit = "<button" + editMethod+">edit</button>";
        var buttonDelete = "<button" + deleteMethod+">delete</button>";

        ul.append(element);
        ul.append(buttonDelete);
        ul.append(buttonEdit);
    }

}
function validateEmail(emailField){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return  reg.test(emailField)
}

//show user in website
function showloggedUsed(res){
    //welcome msg
    var welcomeMsg = "Hello, " + res.name;
    //get p tag for welcome msg
    var welcomeObj = $(".welcomeUser");
    //get container for login
    var headerLogin = $(".headerLogin");
    //set msg
    welcomeObj.text(welcomeMsg);
    //show container
    headerLogin.css("display", "block");
    //hide containers for buttons
    $(".headerButtonsHolder").css("display", "none");
}
function logOut() {
    var headerLogin = $(".headerLogin");
    headerLogin.css("display", "none");
    $(".headerButtonsHolder").css("display", "block");
    $(".adminPanel").css("display", "none");
    user = "";
    goToHome();
}
//hide/show forms
function showSignUp() {
    $("#welcomeHeader").css("display", "none");
    $("#signUpHeader").css("display", "block");
    $("#createUser").css("display", "block");
    $(".productsHolder").css("display","none");

}
function hideSignUp() {
    //hide form
    $("#createUser").css("display", "none");
    $("#loader").css("display", "none");
    $("#welcomeHeader").css("display", "block");
    $(".productsHolder").css("display","block");
    $("#signUpHeader").css("display", "none");



}
function showLogin() {
    $("#signUpHeader").css("display", "none");
    $("#welcomeHeader").css("display", "none");
    $("#loginHeader").css("display", "block");
    $("#loginUser").css("display", "block");
    $(".productsHolder").css("display","none");

}
function hideLogin() {
    $("#welcomeHeader").css("display", "block");
    $("#loginHeader").css("display", "none");
    $("#loginUser").css("display", "none");
    $(".productsHolder").css("display","block");

}
//in account
function setValuesAccount(data) {
    $("#AccName").val(data.name);
    $("#AccLastName").val(data.lastName);
    $("#AccEmail").val(data.email);
    $("#AccPassword").val(data.password);
    $("#AccMobile").val(data.mobile);
    $("#AccPath").val(data.path);
}