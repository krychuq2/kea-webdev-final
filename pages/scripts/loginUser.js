//document ready
$( document ).ready(function() {

    var emailInput = $("#emailLogin");
    var passwordInput = $("#passwordLogin");

    //on loginForm submit
    $( "#loginUser" ).submit(function( event ) {
        var email = {
            val : emailInput.val(),
            minLength : 0,
            id : 'email'
        };
        var password = {
            val : passwordInput.val(),
            minLength : 5,
            id : 'password'
        };
        //array of input controllers
        var inputArr = [email, password];
        //validate form
        if(validateForm(inputArr)){
            loginUser(email.val, password.val);
        }else{
            console.log("invalid")
        }
        event.preventDefault();
    });
    $(".logout").click(function () {
        logOut();
    });

    //on header login click
    $("#showLogin").click(function () {
        hideSignUp();
        showLogin();
    });

});