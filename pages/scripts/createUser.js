//document ready
$( document ).ready(function() {
    //user info
    var nameInput = $("#name");
    var lastNameInput = $("#lastName");
    var emailInput = $("#email");
    var passwordInput = $("#password");
    var mobileInput = $("#mobile");
    var fileInput = $("#file");
    var user = {};

        //on creteUser form submit
    $( "#createUser" ).submit(function( event ) {

        var name = {
            val : nameInput.val(),
            minLength : 3,
            id : 'name'
        };
        var lastName = {
            val : lastNameInput.val(),
            minLength : 3,
            id : 'lastName'
        };
        var email = {
            val : emailInput.val(),
            minLength : 0,
            id : 'email'
        };
        var password = {
            val : passwordInput.val(),
            minLength : 5,
            id : 'password'
        };

        var mobile = {
            val : mobileInput.val(),
            minLength : 5,
            id : 'mobile'
        };
        var file = {
            val : fileInput.val(),
            minLength : 5,
            id : 'file'
        };

        //array of input controllers
        var inputArr = [name, lastName, email, password, mobile, file];
        //user obj
        user = {
            name : name.val,
            lastName : lastName.val,
            email : email.val,
            password : password.val,
            path : file.val,
            mobile : mobile.val
        };

        //validate form
        if(validateForm(inputArr)){
            //from userService.js
            createUser(user);
            //hide form
            hideSignUp();
            //clear form
            clearForm(inputArr);
        }
        event.preventDefault();
    });

    function clearForm(inputs){
        for(var i = 0; i < inputs.length; i++){
            var inputToClear = "#"+inputs[i].id;
            $(inputToClear).val("");
        }

    }
    $("#showRegistration").click(function () {
        hideLogin();
        showSignUp();


    });

});