function goToAccount() {
    //hide products
    $(".productsHolder").css("display","none");

    //show account
    $(".accountPage").css("display", "block");
    //hide homePage
    $(".homePage").css("display", "none");
    //check if user is an admin
    if(user.isAdmin === '1'){
        $("#showAdminPanel").css("display", "block");

    }else{

    }
}

function goToHome() {
    //show products
    $(".productsHolder").css("display","block");

    //show account
    $(".accountPage").css("display", "none");
    //hide homePage
    $(".homePage").css("display", "flex");
    hideLogin();
    hideSignUp();
}
