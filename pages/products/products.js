$( document ).ready(function() {
    var priceSort = false;
    var nameSort = false;
    getProdructs(0);

    $("#getProducts").click(function () {
        getProdructs(1);
        $("#getProducts").css("display","none");
        $("#hideProducts").css("display","inline");
    });
    $("#hideProducts").click(function () {
        var ul = $("#productsUl");
        ul.html('');
        $("#getProducts").css("display","inline");
        $("#hideProducts").css("display","none");
        
    });
    $("#sortByPrice").click(function () {
        priceSort =!priceSort;
        if(priceSort){
            //sort products
            var sorted = products.sort(function(a, b) {
                return  parseInt(a.price) - parseInt(b.price);
            });
        }else{
            //sort products
            var sorted = products.sort(function(a, b) {
                return  parseInt(b.price) - parseInt(a.price);
            });
        }

        setProducts(sorted);
    });
    $("#sortByName").click(function () {
        nameSort =!nameSort;
        if(nameSort){
            var sorted = products.sort(function(a, b) {
                return  (a.name > b.name);
            });
        }else{
            var sorted = products.sort(function(a, b) {
                return  (a.name < b.name);
            });
        }
        setProducts(sorted);
    });

    $("#searchProduct").keyup(function(e){
        setTimeout(function () {
            var value =  $("#searchProduct").val();
            var sorted = [];
            if(value.length !== 0){
                for(var i = 0; i<products.length; i++){
                    if(products[i].name.indexOf(value) !== -1){
                        sorted.push(products[i]);
                    }
                }
                setProducts(sorted);
            }else{
                setProducts(products);
            }
        },50);

    });





    
});