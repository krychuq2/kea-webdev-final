var product;


function setProducts(res) {
    var holder = $("#productsLst");
    holder.html('');


    for(var i = 0; i<res.length; i++){
        var id= '\'' + res[i].id + '\'';
        var name=  res[i].name;
        var price = res[i].price;
        var amount = res[i].amount;
        var buyMethod = " onclick=buy("+id+")";

        var row = "<div id='productRow'><p>"+name+"</p> <p>"+price +" $</p> <p>"+amount+"</p><button"+buyMethod +">buy</button></div>";


        holder.append(row);

    }
}
function buy(id) {
    if(user.email){
       buyProduct(id);
    }else{
        alert("you have to login to buy");
    }
}
function setProductsAdmin(res){
    var ul = $("#productsUl");
    ul.html('');


    for(var i = 0; i<res.length; i++){

        var id= '\'' + res[i].id + '\'';
        var productName = res[i].name;
        var editMethod = " onclick=getProduct("+id+")";
        var deleteMethod = " onclick=deleteProduct("+id+")";

        var element = "<li>"+productName+"</li>";
        var buttonEdit = "<button" + editMethod+">edit</button>";
        var buttonDelete = "<button" + deleteMethod+">delete</button>";

        ul.append(element);
        ul.append(buttonDelete);
        ul.append(buttonEdit);
    }
}

function editProduct(res){
    $(".addProductHolder").css("display","block");
    product = res;
   $("#productName").val(product.name);
   $("#productPrice").val(product.price);
   $("#productPath").val(product.imgPath);
   $("#productAmount").val(product.amount);
   $("#updateProduct").css("display", "inline");
   $("#saveProduct").css("display", "none");

    // var saveButton =  $("#saveProduct");
   //  saveButton.text("update");
   //  saveButton.removeAttr('id');
   //  saveButton.attr('onclick', 'updateProduct()');
}
function hideUpdateForm() {
    $(".addProductHolder").css("display","none");
    $("#updateProduct").css("display", "none");
    $("#saveProduct").css("display", "inline");
}