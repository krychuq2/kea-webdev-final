<?php
/**
 * Created by PhpStorm.
 * User: krysn
 * Date: 30.09.2017
 * Time: 23:49
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../PHPMailer/src/Exception.php';
require '../../PHPMailer/src/PHPMailer.php';
require '../../PHPMailer/src/SMTP.php';

//db connection
$serverName = "localhost";
$username = "root";
$password = "root";
$dbName = "kea";

// Create connection
$con = mysqli_connect($serverName, $username, $password, $dbName);

function updateUser($connection){
    $user = $_POST['updateUser'];
    $name = $user['name'];
    $lastName = $user['lastName'];
    $password = $user['password'];
    $email = $user['email'];

    //sql statement for insert
    $sql = "UPDATE users SET 
      `name` = '$name', 
      `lastName` = '$lastName', 
      `password` = '$password'
      where  email = '$email'";
    if ($connection->query($sql) === TRUE) {
         login($connection, $email, $password);
    } else {
        echo "Error updating record: " . $connection->error;
    }
}

function users($connection){
    //user attributes
    $user = $_POST['user'];
    $name = $user['name'];
    $lastName = $user['lastName'];
    $email = $user['email'];
    $password = $user['password'];
    $mobile = $user['mobile'];
    $path = $user['path'];
    //is admin false on default
    $isAdmin = 0;
    //check if it should be an admin
    $result = mysqli_query($connection, "SELECT * FROM users");
    if ($result->num_rows == 0) {
        //set admin to true
        $isAdmin = 1;
    }
    //sql statement for insert
    $sql = "INSERT INTO users (email, name, lastName, password, isAdmin, mobile, path)
    VALUES ('$email','$name', '$lastName', '$password', '$isAdmin', '$mobile', '$path');";
    //check if sql is ok
    if ($connection->query($sql)) {
        //log des
        $log = date("F j, Y, g:i a")." ". $email . " was created". PHP_EOL;
        //add log to text file
        file_put_contents('data/users.txt', $log, FILE_APPEND);
        //return success
        echo "Success";
    } else {
        http_response_code(404);
        echo "Error: " . $connection->error;
    }
}

function login($connection, $email, $password){
    //sql statement for login
    $result = mysqli_query($connection, "SELECT * FROM users WHERE email = '$email' AND password = '$password'");
    if ($result->num_rows > 0) {
        $log = date("F j, Y, g:i a")." ". $email . " was login". PHP_EOL;
        //add log to text file
        file_put_contents('data/users.txt', $log, FILE_APPEND);

        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($result->fetch_assoc());

    }else{
        http_response_code(404);
        echo "Wrong username or password";
    }
}
function getUsers($connection){
    $user = $_GET['users'];
    $admin = $user['isAdmin'];
    if($admin){
        $users = array();
        //sql statement for login
        $result = mysqli_query($connection, "SELECT * FROM users WHERE isAdmin = 0");
        header('Content-type: application/json');
        while($row = $result->fetch_assoc()) {
            array_push($users,$row);

        }
        echo json_encode($users);

    }else{
        http_response_code(400);
        echo "error";

    }
}
function deleteUser($connection, $email){

    // sql to delete a record
    $sql = "DELETE FROM users WHERE email='$email'";
    header('Content-type: application/json');

    if ($connection->query($sql) === TRUE) {
        $result = mysqli_query($connection, "SELECT * FROM users WHERE isAdmin = 0");
        $users = array();
        while($row = $result->fetch_assoc()) {
            array_push($users,$row);

        }
        echo json_encode($users);
    } else {
        http_response_code(500);
        echo "Error deleting record: " . $connection->error;
    }
}
function getUserAdmin($connection, $email){
    //sql statement for login
    $result = mysqli_query($connection, "SELECT * FROM users WHERE email = '$email'");
    if ($result->num_rows > 0) {

        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($result->fetch_assoc());
    }else{
        http_response_code(404);
        echo "Wrong username or password";
    }
}
function sendEmail(){
$mail = new PHPMailer();

// ---------- adjust these lines ---------------------------------------
$mail->Username = "kea.exam.2017@gmail.com"; // your GMail user name
$mail->Password = "*******"; //dupa123
$mail->AddAddress($_GET['sendEmail']); // recipients email
$mail->FromName = "Kea app"; // readable name

$mail->Subject = "SingUp";
$mail->Body    = "You have been sign up";
//-----------------------------------------------------------------------

$mail->Host = "ssl://smtp.gmail.com"; // GMail
$mail->Port = 465;
$mail->IsSMTP(); // use SMTP
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->From = $mail->Username;
    if(!$mail->Send()){
        echo "Mailer Error: " . $mail->ErrorInfo;

    }else{
        echo "Message has been sent";

    }
}
//get user
if(isset($_GET['getUser'])){
    $user = $_GET['getUser'];
    $email = $user['email'];
    getUserAdmin($con, $email);
}
//send email
if(isset($_GET['sendEmail'])){
    sendEmail();
}
//api createUser
if(isset($_POST['user']))
{
    echo users($con);
}
//api login
if(isset($_POST['login'])){
   $login = $_POST['login'];
   $email = $login['email'];
   $password = $login['password'];
    echo login($con, $email, $password);
}
//update user
if(isset($_POST['updateUser'])){
   echo updateUser($con);
}
//get users
if(isset($_GET['users'])){
    getUsers($con);
}
//delete user
if(isset($_GET['email'])){
    //user attributes
    $user = $_GET['email'];
    $email = $user['email'];
    echo  deleteUser($con, $email);
}

