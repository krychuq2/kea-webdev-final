<?php
/**
 * Created by PhpStorm.
 * User: krysn
 * Date: 04.10.2017
 * Time: 22:11
 */
$serverName = "localhost";
$username = "root";
$password = "";
$dbName = "kea";

// Create connection
$con = mysqli_connect($serverName, $username, $password, $dbName);

// Check connection
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());

}
$result = mysqli_query($con, "SELECT * FROM users");
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["email"];
    }
} else {
    echo "0 results";
}

