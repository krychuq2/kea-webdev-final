<?php
/**
 * Created by PhpStorm.
 * User: krysn
 * Date: 08.10.2017
 * Time: 22:38
 */

//db connection
$serverName = "localhost";
$username = "root";
$password = "root";
$dbName = "kea";

// Create connection
$con = mysqli_connect($serverName, $username, $password, $dbName);

function insertProduct($connection){
    //user attributes
    $product = $_POST['product'];
    $name = $product['name'];
    $price = $product['price'];
    $path = $product['path'];
    $amount = $product['amount'];

    //sql statement for insert
    $sql = "INSERT INTO products (name, price, imgPath, amount)
    VALUES ('$name', $price, '$path', '$amount');";
    //check if sql is ok
    if ($connection->query($sql)) {
        //log des
//        $log = date("F j, Y, g:i a")." ". $email . " was created". PHP_EOL;
//        //add log to text file
//        file_put_contents('data/users.txt', $log, FILE_APPEND);
        //return success
        echo "Success";
    } else {
        http_response_code(404);
        echo "Error: " . $connection->error;
    }
}
function getProducts($connection){
    $products = array();
    //sql statement for login
    $result = mysqli_query($connection, "SELECT * FROM products");
    header('Content-type: application/json');
    while($row = $result->fetch_assoc()) {
        array_push($products,$row);

    }
    echo json_encode($products);

}
function deleteProduct($connection, $id){
    // sql to delete a record
    $sql = "DELETE FROM products WHERE id='$id'";

    if ($connection->query($sql) === TRUE) {
        getProducts($connection);
    } else {
        http_response_code(500);
        echo "Error deleting record: " . $connection->error;
    }
}
function getProduct($connection, $id){
    //sql statement for login
    $result = mysqli_query($connection, "SELECT * FROM products WHERE id = '$id'");
    if ($result->num_rows > 0) {

        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($result->fetch_assoc());
    }else{
        http_response_code(404);
        echo "Wrong username or password";
    }
}
function updateProduct($connection, $data){
    $name = $data['name'];
    $price = $data['price'];
    $path = $data['imgPath'];
    $amount = $data['amount'];
    $id = $data['id'];
    //sql statement for insert
    $sql = "UPDATE products SET
      `name` = '$name',
      `price` = '$price',
      `imgPath` = '$path',
      `amount` = '$amount',
      `id` = '$id'

      where  id = '$id'";

    if ($connection->query($sql) === TRUE) {
            getProducts($connection);
    } else {
        echo "Error updating record: " . $connection->error;
    }

}
//add product
if(isset($_POST['product']))
{
    insertProduct($con);
}

//get products
if(isset($_GET['products'])){
    getProducts($con);
}
//delete user
if(isset($_GET['id'])){
    //user attributes
    $id = $_GET['id'];
    deleteProduct($con, $id);
}
//get products
if(isset($_GET['getProduct'])){
    $id = $_GET['getProduct'];
    getProduct($con, $id);
}
//update users
if(isset($_POST['updateProduct'])){
    $product = $_POST['updateProduct'];
    updateProduct($con, $product);
}