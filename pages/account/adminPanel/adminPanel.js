//document ready
$( document ).ready(function() {
    //inputs
    var productName = $("#productName");
    var productPrice = $("#productPrice");
    var productPath = $("#productPath");
    var productAmount = $("#productAmount");
    //controllers
    var productNameController;
    var productPriceController;
    var productPathController;
    var productAmountController;

    $("#adminGetUsers").click(function () {
        $("#hideUsers").css("display", "block");
        $("#adminGetUsers").css("display", "none");

        getUsers();

    });
    $("#hideUsers").click(function () {
        $("#hideUsers").css("display", "none");
        $("#adminGetUsers").css("display", "block");

        var ul = $("#usersUl");
        ul.html('');
    });

    $("#cancelBtn").click(function () {
        $(".editPage").css("display", "none");
    });
    $("#addProduct").click(function () {
        $(".addProductHolder").css("display", "block");
    });

    //products
    $("#saveProduct").click(function () {
      if(validateProduct()){
          insertProduct(setProduct());
      }

    });
    //update product
    $("#updateProduct").click(function () {
        if(validateProduct()){
            var produteToUpdate = setProduct();
            produteToUpdate.id = product.id;
            updateProduct(produteToUpdate);
        }
    });

    function validateProduct(){
         productNameController = {
            val : productName.val(),
            minLength : 3,
            id : 'productName'
        };
         productPriceController = {
            val : productPrice.val(),
            minLength : 1,
            id : 'productPrice'
        };
         productPathController = {
            val : productPath.val(),
            minLength : 4,
            id : 'productPath'
        };
         productAmountController = {
            val : productAmount.val(),
            minLength : 1,
            id : 'productAmount'
        };
        //array of input controllers
        var inputArr = [productNameController, productPriceController, productPathController, productAmountController];
        //validate form
        return validateForm(inputArr);
    }

    function setProduct() {
        var product = {
            name : productNameController.val,
            price : productPriceController.val,
            path : productPathController.val,
            amount : productAmountController.val

        };
        return product;
    }

});
