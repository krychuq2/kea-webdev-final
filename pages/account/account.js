$( document ).ready(function() {
    //on header button click
    var nameInput = $("#AccName");
    var lastNameInput = $("#AccLastName");
    var emailInput = $("#AccEmail");
    var passwordInput = $("#AccPassword");
    var mobileInput = $("#AccMobile");
    var pathInput = $("#AccPath");

    $(".welcomeUser").click(function () {
        goToAccount();
        //set values to inputs
        console.log(getLocalUser(), "this is user on hello click");
        setValuesAccount(getLocalUser());
    });

    $("#showEditPage").click(function () {
        showEdit();
    });
    $("#hideEditPage").click(function () {
        hideEdit();
    });

    $("#showAdminPanel").click(function () {
        showAdmin();
    });
    $("#hideAdminPanel").click(function () {
        hideAdmin();
    });
    $("#editBtn").click(function () {
        //validate form
        var name = {
            val : nameInput.val(),
            minLength : 3,
            id : 'name'
        };
        var lastName = {
            val : lastNameInput.val(),
            minLength : 3,
            id : 'lastName'
        };
        var email = {
            val : emailInput.val(),
            minLength : 0,
            id : 'email'
        };
        var password = {
            val : passwordInput.val(),
            minLength : 5,
            id : 'password'
        };
        var mobile = {
            val : mobileInput.val(),
            minLength : 5,
            id : 'mobile'
        };
        var file = {
            val : pathInput.val(),
            minLength : 5,
            id : 'file'
        };
        //array of input controllers
        var inputArr = [name, lastName, email, password, mobile, file];

        //validate form
        if(validateForm(inputArr)){
            var updatedUser = {
                name : name.val,
                lastName : lastName.val,
                email : email.val,
                password : password.val,
                path : file.val,
                mobile : mobile.val
            };
            updateUser(updatedUser);

        }

    });
    function showEdit() {
        getUserAdmin(getLocalUser().email);
        $(".editPage").css("display", "block");
        $("#showEditPage").css("display", "none");
        $("#hideEditPage").css("display", "block");

    }
    function hideEdit() {
        $(".editPage").css("display", "none");
        $("#showEditPage").css("display", "block");
        $("#hideEditPage").css("display", "none");
    }

    function showAdmin(){
        $(".adminPanel").css("display", "block");
        $("#hideAdminPanel").css("display", "block");
        $("#showAdminPanel").css("display", "none");
        $(".editPage").css("display", "none");
    }
    function hideAdmin() {
        $(".adminPanel").css("display", "none");
        $("#hideAdminPanel").css("display", "none");
        $("#showAdminPanel").css("display", "block");


    }

    $(".home").click(function () {
        console.log("it should go to home");
        goToHome();
    });


});