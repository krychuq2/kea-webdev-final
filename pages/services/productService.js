var products;

function insertProduct(product) {
    request = $.ajax({
        url: "../backend/products.php",
        type: "post",
        data: {'product': product},
        success: insertProductSuccess
    });

    function insertProductSuccess(res) {
        $(".addProductHolder").css("display", "none");
        //update dashboard
        getProdructs();

    }
}

function getProdructs(admin) {
    request = $.ajax({
        url: "../backend/products.php",
        type: "get",
        data: {'products': 1},
        success: productsSuccess
    });

    function productsSuccess(res) {
        products = res;
        if(admin===1){
            setProductsAdmin(res);
            setProducts(res);

        }else {
            setProducts(res);

        }

    }
}
function getProduct(id){
    request = $.ajax({
        url: "../backend/products.php",
        type: "get",
        data: {'getProduct': id},
        success: getProductsSuccess,
        error : getProductsError
    });

    function getProductsSuccess(res) {
        editProduct(res);
    }
    function getProductsError() {
        console.log("err, err, err")

    }
}
function buyProduct(id) {
    var producToBuy = getProductLocal(id);
    if(producToBuy.amount - 1 >0){
        producToBuy.amount = producToBuy.amount - 1;
        updateProduct(producToBuy);
    }else{
        alert("there is no products in stock");
    }


}

function getProductLocal(id) {
    for(var i = 0; i <products.length; i++){
        if(products[i].id === id){
            return products[i];
        }
    }

}
function updateProduct(data) {
    request = $.ajax({
        url: "../backend/products.php",
        type: "post",
        data: {'updateProduct': data},
        success: updateSuccess
    });
    function updateSuccess(res) {
        setProductsAdmin(res);
        setProducts(res);
        hideUpdateForm();
    }
}
function deleteProduct(id) {
    request = $.ajax({
        url: "../backend/products.php",
        type: "get",
        data: {'id': id},
        success: deleteSuccess,
        error : deleteError
    });

    function deleteSuccess() {
        getProdructs(1);
    }
    function deleteError() {

    }
}