
   var user = "";

    function createUser(user) {
        $("#loader").css("display", "block");
        $("#signUpHeader").css("display", "none");
        request = $.ajax({
            url: "../backend/users.php",
            type: "post",
            data: {'user': user},
            success: registerSuccess
        });

        function registerSuccess(res) {
            hideSignUp();
            sendEmail(user.email);
            //after registration in user should log into system
            loginUser(user.email, user.password);
        }
    }
    function updateUser(data){
        request = $.ajax({
            url: "../backend/users.php",
            type: "post",
            data: {'updateUser': data},
            success: updateSuccess
        });
        function updateSuccess(res) {
            user = res;
            setValuesAccount(res);
        }

    }
    function sendEmail(email) {
        request = $.ajax({
            url: "../backend/users.php",
            type: "get",
            data: {'sendEmail': email},
            success: sendSuccess,
            error : sendError
        });

        function sendSuccess(res) {
            console.log(res);
        }
        function sendError() {

        }
    }
    function loginUser(email, password){
        request = $.ajax({
            url: "../backend/users.php",
            type: "post",
            data: {'login': {'email' : email, 'password' : password}},
            success: loginSuccess,
            error : loginError
        });

        function loginSuccess(res) {
            user = res;
            console.log(user);
            afterLogin(res);
        }
        function loginError() {
            $(".loginError").css("display", "block");
        }
    }
    function getUserAdmin(email){
        request = $.ajax({
            url: "../backend/users.php",
            type: "get",
            data: {'getUser': {'email' : email}},
            success: getUseSuccess,
            error : getUserError
        });

        function getUseSuccess(res) {
            setValuesAccount(res);
        }
        function getUserError() {

        }
    }
    function deleteUser(email) {
        request = $.ajax({
            url: "../backend/users.php",
            type: "get",
            data: {'email': {'email' : email}},
            success: deleteSuccess,
            error : deleteError
        });

        function deleteSuccess(res) {
            setUsersToDashBoard(res);
        }
        function deleteError() {

        }
    }

    function getUsers() {
        request = $.ajax({
            url: "../backend/users.php",
            type: "get",
           // data: {'users': {'isAdmin' : user.isAdmin}},
            data: {'users': {'isAdmin' : 1}},
            success: usersSuccess
        });

        function usersSuccess(res) {
            setUsersToDashBoard(res);

        }
    }

    function afterLogin(res) {
        hideLogin();
        showloggedUsed(res);

    }
    function getLocalUser() {
        return user;
    }